package com.codigonline;
import com.mongodb.MongoException;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class MongoDBManager {
    private MongoClient mongoClient;
    private MongoDatabase database;
    private MongoCollection<Document> collection;

    public MongoDBManager(String uri ,String databaseName, String collectionName) throws MongoException {
        try {
            MongoClient mongoClient = MongoClients.create(uri);
            this.database = mongoClient.getDatabase(databaseName);
            this.collection = database.getCollection(collectionName);

        } catch (MongoException e) {
            throw new MongoException("Error al conectar con MongoDB: " + e.getMessage());
        }

    }

    public void insertDocument(Document document) throws MongoException {
        try {
            collection.insertOne(document);
            System.out.printf("dato insetado correctamente");
        } catch (MongoException e) {
            throw new MongoException("Error al insertar el documento en MongoDB: " + e.getMessage());

        }
    }

    public Document findDocument(Document query) throws MongoException {
        try {
            return collection.find(query).first();

        } catch (MongoException e) {
            throw new MongoException("Error al buscar el documento en MongoDB: " + e.getMessage());
        }
    }

    public void updateDocument(Document query, Document update) throws MongoException {
        try {
            collection.updateOne(query, new Document("$set", update));
        } catch (MongoException e) {
            throw new MongoException("Error al actualizar el documento en MongoDB: " + e.getMessage());
        }
    }

    public void deleteDocument(Document query) throws MongoException {
        try {
            collection.deleteOne(query);
        } catch (MongoException e) {
            throw new MongoException("Error al eliminar el documento en MongoDB: " + e.getMessage());
        }
    }

    public void close() {
        if (mongoClient != null) {
            mongoClient.close();
        }
    }
}
